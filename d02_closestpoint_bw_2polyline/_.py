"""
ref gemini https://g.co/gemini/share/6813573d94c0
"""
from pathlib import Path

import folium
from shapely.geometry import Point
from geopy.distance import geodesic

from d01_taxiway_.v1.taxiway_list import taxiway_byname_d
from d01_taxiway_.helper import get_center_loc


#region bw two polylines
def find_closest_point(polyline1, polyline2):
  """
  This function takes two polylines (lists of coordinates)
  and finds the closest points bw them
  ref gemini https://g.co/gemini/share/6813573d94c0
  """
  shortest_distance = float('inf')

  closest_p1, closest_p2 = None,None
  for p1 in polyline1:
    for p2 in polyline2:
      distance = geodesic(p1, p2).m  # calculate distance in meters
      if distance < shortest_distance:
        shortest_distance = distance
        closest_p1        = Point(p1)
        closest_p2        = Point(p2)

  # return closest_p1, closest_p2
  debug=122
  return closest_p1.coords[0], closest_p2.coords[0]


def plot_closest_point(polyline1, polyline2, output_p=None):
  closest_p1, closest_p2 = find_closest_point(polyline1, polyline2)

  m = folium.Map(location=get_center_loc([closest_p1, closest_p2]), zoom_start=18)

  # plot polyline
  folium.PolyLine(locations=polyline1, color='red').add_to(m)
  folium.PolyLine(locations=polyline2, color='green').add_to(m)

  # plot the two closest
  folium.PolyLine(locations=[closest_p1, closest_p2], color='blue').add_to(m)
  folium.Marker(location=closest_p1,       icon=folium.Icon('blue')).add_to(m)
  folium.Marker(location=closest_p2,       icon=folium.Icon('blue')).add_to(m)

  if not output_p: output_p=Path(__file__).parent /f'{Path(__file__).stem}__{find_closest_point.__name__}.html'
  m.save(output_p)
#endregion bw two polylines


#region bw many polylines
def plot_closest_point__multi(polyline_list, output_p=None):
  n = len(polyline_list)

  #region get center_loc
  closest_ijpair_all = []
  for at_i   in range(0,     n):
    for at_j in range(at_i+1,n):
      closest_i,closest_j = find_closest_point(polyline_list[at_i],polyline_list[at_j])
      closest_ijpair_all.append([closest_i,closest_j])
  closest_point_all = [loc for pair in closest_ijpair_all for loc in pair]
  center_loc=get_center_loc(closest_point_all)
  #endregion get center_loc

  m = folium.Map(location=center_loc, zoom_start=17)

  # plot polyline
  for at_i in range(0, n):
    folium.PolyLine(locations=polyline_list[at_i], color='blue').add_to(m)

  # plot closest point/line in pairofpolyline
  for at_i   in range(0,     n):
    for at_j in range(at_i+1,n):
      closest_i,closest_j = find_closest_point(polyline_list[at_i],polyline_list[at_j])

      folium.PolyLine(locations=[closest_i, closest_j], color='red',weight=1).add_to(m)

      'turn on below lines to plot the two ends of the closest line'
      # folium.Marker(location=closest_i,      icon=folium.Icon('blue')).add_to(m)
      # folium.Marker(location=closest_j,      icon=folium.Icon('blue')).add_to(m)

  #region plot junction area
  'get junction area fr :closest_ijpair_all - middle point of closest lines'
  junction_area_loc_list=[get_center_loc(pair) for pair in closest_ijpair_all]

  '''plot polygon ref https://g.co/gemini/share/508407578aa3 '''
  # add the polygon to the map with defined style
  folium.PolyLine(locations=junction_area_loc_list,
                  weight=0, fill_color='#f00', fill_opacity=.5).add_to(m)
  #endregion plot junction area


  # output map to html
  if not output_p: output_p=Path(__file__).parent /f'{Path(__file__).stem}__{find_closest_point.__name__}.html'
  m.save(output_p)

def demo__plot_closest_point__multi(taxiway_names):
  plot_closest_point__multi([taxiway_byname_d[n] for n in taxiway_names],
                            output_p=Path(__file__).parent/f'zo/{Path(__file__).stem}__{demo__plot_closest_point__multi.__name__}__{"_".join(taxiway_names)}.html')
#endregion bw many polylines


#TODO add tooltip=taxiway_name/closest_polyline
if __name__ == "__main__":
  # plot_closest_point(polyline1=taxiway_byname_d['V'],   polyline2=taxiway_byname_d['S3'] , output_p=Path(__file__).parent/f'zo/{Path(__file__).stem}__{find_closest_point.__name__}__V_S3.html')
  # plot_closest_point(polyline1=taxiway_byname_d['V10'], polyline2=taxiway_byname_d['S3'],  output_p=Path(__file__).parent/f'zo/{Path(__file__).stem}__{find_closest_point.__name__}__V10_S3.html')
  # plot_closest_point(polyline1=taxiway_byname_d['V10'], polyline2=taxiway_byname_d['V'] ,  output_p=Path(__file__).parent/f'zo/{Path(__file__).stem}__{find_closest_point.__name__}__V10_V.html')

  # plot_closest_point__multi([taxiway_byname_d['V'],   taxiway_byname_d['S3'] ],  output_p=Path(__file__).parent/f'zo/{Path(__file__).stem}__{plot_closest_point__multi.__name__}__V_S3.html')
  # plot_closest_point__multi([taxiway_byname_d['V10'], taxiway_byname_d['S3'] ],  output_p=Path(__file__).parent/f'zo/{Path(__file__).stem}__{plot_closest_point__multi.__name__}__V10_S3.html')
  # plot_closest_point__multi([taxiway_byname_d['V10'], taxiway_byname_d['V'] ],   output_p=Path(__file__).parent/f'zo/{Path(__file__).stem}__{plot_closest_point__multi.__name__}__V10_V.html')

  # plot_closest_point__multi([taxiway_byname_d['V'],
  #                            taxiway_byname_d['W'],
  #                            taxiway_byname_d['S2'],
  #                            ],  output_p=Path(__file__).parent/f'zo/{Path(__file__).stem}__{plot_closest_point__multi.__name__}__V_W_S2.html')

  # demo__plot_closest_point__multi(['S','V10'])  #FIXME the junction is away from two-line-intersect geometry-ly

  # demo__plot_closest_point__multi(['S','R7','R8'])  # aka result2
  demo__plot_closest_point__multi(['V','W', 'S2'])  # aka result3
