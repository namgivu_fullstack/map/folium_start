from statistics import mean


#region dec2float helper
def dec2float(dec):
  """
  ref gemini https://g.co/gemini/share/65a168f20429
  Converts a decimal.Decimal object to a float with minimal loss.

  This function attempts to convert a decimal.Decimal object to a float with
  minimal loss by using the `quantize()` method to set the desired number of
  decimal places before performing the conversion. This approach helps to minimize
  rounding errors that can occur during direct conversion.

  Returns:
    The closest float representation of the decimal with minimal loss.
  """

  # quantize the decimal to the desired number of decimal places
  decimal_places = 10
  import decimal; quantized_dec = dec.quantize(decimal.Decimal('1e-%s' % decimal_places))

  return float(quantized_dec)


def dec2float_latlon(latlon):
  return [dec2float(latlon['lat']),
          dec2float(latlon['lon']), ]

#endregion dec2float helper

def get_center_latlon(latlon_list):
  return {
    'lat': mean([latlon['lat'] for latlon in latlon_list]),
    'lon': mean([latlon['lon'] for latlon in latlon_list]),
  }

def get_center_loc(loc_list):
  return [
    mean(loc_t[0] for loc_t in loc_list),
    mean(loc_t[1] for loc_t in loc_list),
  ]
