import folium
from pathlib import Path

from taxiway_list import taxiway_byname_d
from d01_taxiway_.helper import get_center_loc


def plot_taxiway(taxiway_name):
  loc_list = taxiway_byname_d[taxiway_name]
  m        = folium.Map(location=get_center_loc(loc_list), zoom_start=15)

  folium.PolyLine(loc_list, tooltip=taxiway_name).add_to(m)

  m.save(Path(__file__).parent/f'zo/{Path(__file__).stem}__{plot_taxiway.__name__}__{taxiway_name}.html')


if __name__ == '__main__':
  # plot_taxiway('V')
  # plot_taxiway('V10')
  # plot_taxiway('S3')

  # plot_taxiway('W')

  plot_taxiway('S')
  plot_taxiway('V10')
