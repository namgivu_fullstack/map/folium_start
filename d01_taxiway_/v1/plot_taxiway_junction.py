from pathlib import Path
#
import folium
from shapely.geometry import LineString
#
from d01_taxiway_.helper import get_center_loc
from taxiway_list import taxiway_byname_d
#
from d01_taxiway_.service.folium_service import plot_polyline, plot_marker


def get_line_intersect(loc_list1, loc_list2):
  """ref https://g.co/gemini/share/317bece1ec66 """
  l1    = LineString(loc_list1)
  l2    = LineString(loc_list2)
  i_obj = l1.intersection(l2)

  if i_obj.geom_type == 'Point':
    loc = i_obj.__geo_interface__['coordinates']
    r = [loc]
  elif i_obj.geom_type == 'MultiPoint':
    r = [loc for loc in i_obj.__geo_interface__['coordinates'] ]
  else:
    raise Exception(f'Unsupported type {i_obj.geom_type}')

  return r


def plot_2taxiway_intersect(taxiway_name1, taxiway_name2):
  loc_list1 = taxiway_byname_d[taxiway_name1]
  loc_list2 = taxiway_byname_d[taxiway_name2]

  l1 = LineString(loc_list1)
  l2 = LineString(loc_list2)

  i_loc_list = get_line_intersect(loc_list1, loc_list2)

  # Create base map
  m = folium.Map(location=get_center_loc(loc_list1+loc_list2), zoom_start=16)

  folium.PolyLine(l1.coords, color='blue').add_to(m)
  folium.PolyLine(l2.coords, color='blue').add_to(m)

  for loc in i_loc_list:
    folium.Marker(location=loc, icon=folium.Icon('black')).add_to(m)

  m.save( Path(__file__).parent/f'zo/{Path(__file__).stem}__{plot_2taxiway_intersect.__name__}__{taxiway_name1}_{taxiway_name2}.html' )


def plot_partial_taxiway(main_taxiwayname, partial_start_at_taxiwayname, partial_end_at_taxiwayname):
  main, startat, endat = (taxiway_byname_d[main_taxiwayname],
                          taxiway_byname_d[partial_start_at_taxiwayname],
                          taxiway_byname_d[partial_end_at_taxiwayname])

  i1_loc_list = get_line_intersect(main, startat)
  i2_loc_list = get_line_intersect(main, endat)

  m = folium.Map(location=get_center_loc(main+startat+endat), zoom_start=16)

  plot_polyline(m, main,    tooltip=main_taxiwayname)
  plot_polyline(m, startat, tooltip=partial_start_at_taxiwayname)
  plot_polyline(m, endat,   tooltip=partial_end_at_taxiwayname)

  for loc in i1_loc_list: plot_marker(m, loc, color='black', tooltip=f'{main_taxiwayname} x {partial_start_at_taxiwayname}')
  for loc in i2_loc_list: plot_marker(m, loc, color='black', tooltip=f'{main_taxiwayname} x {partial_end_at_taxiwayname}')

  m.save( Path(__file__).parent/f'zo/{Path(__file__).stem}__{plot_partial_taxiway.__name__}__{main_taxiwayname}_{partial_start_at_taxiwayname}_{partial_end_at_taxiwayname}.html' )


if __name__=='__main__':
  # plot_2taxiway_intersect('S','V10')  # aka result4

  # aka result2
  plot_partial_taxiway('S','R7', 'R8')
