from d01_taxiway_.helper import dec2float
from d01_taxiway_.zref.openstreetmap_start.zo_ls_taxiway_WSSS import taxiway_d__list as taxiway_byname_d__latlon


taxiway_byname_d = dict()
for taxiway_name,d in taxiway_byname_d__latlon.items():
  taxiway_byname_d[taxiway_name] = [
    [
      dec2float(latlon['lat']),
      dec2float(latlon['lon']),
    ]
    for latlon in d
  ]


if __name__ == '__main__':
  print(len(taxiway_byname_d['W']))
