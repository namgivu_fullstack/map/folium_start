from pathlib import Path
import folium
#
from d01_taxiway_.helper import get_center_latlon, dec2float_latlon
from d01_taxiway_.zref.openstreetmap_start.zo_ls_taxiway_WSSS import taxiway_d__list


def demo_folium_at_taxiway_W():
  taxiway_name        = 'W'
  taxiway_latlon_list = taxiway_d__list[taxiway_name]['nodes']

  #region init_loc
  taxiway_latlon_0th = taxiway_latlon_list[0]
  folium_0th_loc     = dec2float_latlon(taxiway_latlon_0th)


  taxiway_latlon_center = get_center_latlon(taxiway_latlon_list)
  folium_center_loc     = dec2float_latlon(taxiway_latlon_center)

  init_loc = folium_0th_loc
  init_loc = folium_center_loc  # center looks better
  #endregion init_loc

  m = folium.Map(location=init_loc, zoom_start=16)

  '';                       folium_polylinez_locs = [dec2float_latlon(latlon) for latlon in taxiway_latlon_list]
  folium.PolyLine(locations=folium_polylinez_locs, tooltip=taxiway_name).add_to(m)

  m.save( Path(__file__).parent/f'{Path(__file__).stem}__{demo_folium_at_taxiway_W.__name__}.html' )


def demo_folium_at_taxiway_V_V10_S3():
  """
  we want to draw w/ folium same as overpass at https://overpass-turbo.eu/s/1LJM
  """
  V   = V10_taxiway_latlon_list = taxiway_d__list['V']  ['nodes']
  V10 = V10_taxiway_latlon_list = taxiway_d__list['V10']['nodes']
  S3  = S3_taxiway_latlon_list = taxiway_d__list['S3'] ['nodes']

  #region compute the init_loc
  taxiway_latlon_center = get_center_latlon(  V10_taxiway_latlon_list
                                            + V10_taxiway_latlon_list
                                            + S3_taxiway_latlon_list)
  folium__taxiway_latlon_center = dec2float_latlon(taxiway_latlon_center)

  init_loc = folium__taxiway_latlon_center
  #endregion compute the init_loc

  m = folium.Map(location=init_loc, zoom_start=15)

  folium_V   = [dec2float_latlon(latlon) for latlon in V]
  folium_V10 = [dec2float_latlon(latlon) for latlon in V10]
  folium_S3  = [dec2float_latlon(latlon) for latlon in S3]
  folium.PolyLine(locations=folium_V)  .add_to(m)
  folium.PolyLine(locations=folium_V10).add_to(m)
  folium.PolyLine(locations=folium_S3) .add_to(m)

  m.save( Path(__file__).parent/f'{Path(__file__).stem}__{demo_folium_at_taxiway_V_V10_S3.__name__}.html' )


#region plot_taxiway
def plot_taxiway(taxiway_name, output_p=None):
  taxiway_latlon_list = taxiway_d__list[taxiway_name]['nodes']
  center_latlon = get_center_latlon(taxiway_latlon_list)

  loc_list   = [dec2float_latlon(latlon) for latlon in taxiway_latlon_list]
  center_loc =  dec2float_latlon(center_latlon)

  m = folium.Map(location=center_loc, zoom_start=15)
  folium.PolyLine(loc_list).add_to(m)

  if not output_p: output_p = Path(__file__).parent/f'{Path(__file__).stem}__{plot_taxiway.__name__}__{taxiway_name}.html'
  m.save(output_p)


def plot_taxiway_list(taxiway_name_list):
  '';                                                                             latlon_list=[latlon for taxiway_name in taxiway_name_list for latlon in taxiway_d__list[taxiway_name]['nodes']]
  '';                     center_loc =  dec2float_latlon(latlon=get_center_latlon(latlon_list))
  m = folium.Map(location=center_loc, zoom_start=15)

  for taxiway_name in taxiway_name_list:
    #          = taxiway_latlon_list                                  of taxiway_name
    loc_list   = [dec2float_latlon(latlon) for latlon in taxiway_d__list[taxiway_name]['nodes'] ]
    folium.PolyLine(loc_list, tooltip=taxiway_name).add_to(m)

  m.save(Path(__file__).parent/f'{Path(__file__).stem}'
                               f'__{plot_taxiway_list.__name__}'
                               f'__{"_".join(taxiway_name_list)}.html')

#endregion plot_taxiway


if __name__ == '__main__':
  # demo_folium_at_taxiway_W()
  # demo_folium_at_taxiway_V_V10_S3()

  # plot_taxiway('V')
  # plot_taxiway('V10')
  # plot_taxiway('S3')
  plot_taxiway_list(['V','V10','S3'])
  plot_taxiway_list(['V','W',  'S2'])
