import folium


def plot_polyline(m, loc_list, tooltip=None):
  folium.PolyLine(loc_list, tooltip=tooltip).add_to(m)


def plot_marker(m, loc, tooltip=None, color='blue'):
  folium.Marker(location=loc, icon=folium.Icon(color), tooltip=tooltip).add_to(m)
