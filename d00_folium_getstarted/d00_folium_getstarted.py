from pathlib import Path
import folium


'''
ref https://python-visualization.github.io/folium/latest/getting_started.html#Vectors-such-as-lines
'''
m = folium.Map(location=[-71.38, -73.9], zoom_start=11)

trail_coordinates = [
  (-71.351871840295871, -73.655963711222626),
  (-71.374144382613707, -73.719861619751498),
  (-71.391042575973145, -73.784922248007007),
  (-71.400964450973134, -73.851042243124397),
  (-71.402411391077322, -74.050048183880477),
]

folium.PolyLine(trail_coordinates, tooltip="Coast").add_to(m)

'output as .html  ref https://g.co/gemini/share/e9e682f2d4a3'
m.save( Path(__file__).parent/f'{Path(__file__).stem}.html' )
