gg python to plot a coordinate of lat lng values on a visual map
-> ref https://stackoverflow.com/a/53233489/24512751
-> folium
  -> gg python folium
  -> plot lines/vectors https://python-visualization.github.io/folium/latest/getting_started.html#Vectors-such-as-lines
